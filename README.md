# README #
This code was written for Arduino mini Pro (3.3V - 8MHz). Interfaced with Neo Pixel LED rings it plays four animations for a graduation cap.

### Important note ###

* Adafruit_NeoPixel.h library was used
* It is highly advisable that you go through [their tutorial](https://learn.adafruit.com/adafruit-neopixel-uberguide/overview)
* Check the result [here](https://www.youtube.com/watch?v=Bh-glHXmJ-Q). 

### Repo owner ###
* Fahad Mirza